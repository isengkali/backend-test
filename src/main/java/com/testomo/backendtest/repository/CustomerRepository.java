package com.testomo.backendtest.repository;

import com.testomo.backendtest.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
public interface CustomerRepository extends JpaRepository<Customer,Long> {
}
