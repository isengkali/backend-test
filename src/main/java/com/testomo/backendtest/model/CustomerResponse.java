package com.testomo.backendtest.model;

import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CustomerResponse implements Serializable {

    private static final long serialVersionUID = -5301128489329064139L;

    private String status;
    private Long id;
    private String name;
    private String address;
}
