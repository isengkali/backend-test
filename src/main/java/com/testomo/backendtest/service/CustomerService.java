package com.testomo.backendtest.service;

import com.testomo.backendtest.exception.CustomerException;
import com.testomo.backendtest.model.Customer;
import com.testomo.backendtest.model.CustomerResponse;
import com.testomo.backendtest.repository.CustomerRepository;
import com.testomo.backendtest.validator.CustomerValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

@Service
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final CustomerValidator customerValidator;

    @Autowired
    public CustomerService(CustomerValidator customerValidator, CustomerRepository customerRepository){
        this.customerValidator =  customerValidator;
        this.customerRepository = customerRepository;
    }

    public CustomerResponse newCustomer(Customer customer) {
        CustomerResponse response = new CustomerResponse();
        String msg = customerValidator.validate(customer);
        if(msg != null)throw new CustomerException(msg);
        customer.setName(customer.getName().toLowerCase(Locale.ROOT));
        Customer result = customerRepository.saveAndFlush(customer);
        if(result != null){
            response.setAddress(result.getAddress());
            response.setName(result.getName());
            response.setId(result.getId());
            response.setStatus("True");
        }else{
            response.setAddress(customer.getAddress());
            response.setName(customer.getName());
            response.setId(customer.getId());
            response.setStatus("False");
        }
        return response;
    }

    public HashMap<String, Object> getCustommerAll() {
        List<Customer> list = customerRepository.findAll();
        HashMap<String, Object> returnData = new HashMap<>();
        List<HashMap<String, Object>> temp = new ArrayList<>();
        for(Customer customer : list){
            HashMap<String, Object> keyValue = new HashMap<>();
            keyValue.put("id", customer.getId());
            keyValue.put("name", customer.getName());
            keyValue.put("address", customer.getAddress());
            temp.add(keyValue);
        }
        returnData.put("data", temp);
        return returnData;
    }


}
