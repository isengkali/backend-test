package com.testomo.backendtest.validator;

import com.testomo.backendtest.model.Customer;
import org.springframework.stereotype.Component;

@Component
public class CustomerValidator {
    public String validate(Customer customer){
        if(customer.getName() == null) return "Name tidak boleh kosong";
        if(customer.getAddress() == null) return "Address tidak boleh kosong";
        return null;
    }
}
