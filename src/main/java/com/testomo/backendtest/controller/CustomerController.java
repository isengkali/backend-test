package com.testomo.backendtest.controller;

import com.testomo.backendtest.model.Customer;
import com.testomo.backendtest.model.CustomerResponse;
import com.testomo.backendtest.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping(value = "/api/", produces = MediaType.APPLICATION_JSON_VALUE)
public class CustomerController {
    private final CustomerService customerService;
    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping(value = "customer")
    public CustomerResponse newCustomer(@RequestBody Customer customer){
        return customerService.newCustomer(customer);
    }

    @GetMapping(value = "customers")
    public HashMap<String, Object> findAllCustomers(){
        return customerService.getCustommerAll();
    }




}
