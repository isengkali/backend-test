package com.testomo.backendtest.exception;

import com.testomo.backendtest.util.StatusCode;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CustomerException extends RuntimeException {

	private static final long serialVersionUID = 5671645464112773305L;
	private StatusCode code = StatusCode.ERROR;
    private String errorCode = "ERROR";

    public CustomerException(){
		super();
	}

    public CustomerException(String message){
		super(message);
		log.info("ERROR: {}",message);
	}

    public CustomerException(StatusCode code, String message) {
        super(message);
        this.code = code;
        log.info("ERROR: {}",message);
    }

    public CustomerException(String message, String errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public StatusCode getCode() {
        return code;
    }

    public void setCode(StatusCode code) {
        this.code = code;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

}
