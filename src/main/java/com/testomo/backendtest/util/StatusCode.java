package com.testomo.backendtest.util;

public enum StatusCode {
    OK, ERROR, DATA_NOT_FOUND
}
